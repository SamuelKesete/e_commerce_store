import { Grid } from "@mui/material";
import { Product } from "../../app/models/Product";
import ProductCard from "./ProductCard";


export default function ProductList({ products }: productListProps) {
    return (
        <Grid container spacing={4}>
            {products.map(product => (
                <Grid item xs={3} key={product.id}>
                    <ProductCard product={product} />
                </Grid>


            ))}
        </Grid>
    )
}

interface productListProps {
    products: Product[];
}
