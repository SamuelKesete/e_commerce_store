import { createTheme, CssBaseline, ThemeProvider } from '@mui/material';
import { Container } from "@mui/system";
import { useState } from 'react';
import { Route } from 'react-router-dom';
import AboutPage from '../../features/about/AboutPage';
import Catalog from "../../features/catalog/Catalog";
import ProductDetails from '../../features/catalog/ProductDetails';
import ContactPage from '../../features/contact/ContactPage';
import HomePage from '../../features/home/HomePage';
import LoginPage from '../../features/LoginPage';
import Header from './Header';



function App() {

  const [darkMode, setDarkMode] = useState(false);
  const palettType = darkMode ? 'dark' : 'light';
  const theme = createTheme({
    palette: {
      mode: palettType,
      background: {
        default: palettType === 'light' ? '#d9d9d9' : '#0d0d0d'
      }
    }
  })



  function handleThemeChange() {
    setDarkMode(!darkMode);
  }
  return (
    <>

      <ThemeProvider theme={theme} >
        <CssBaseline />
        <Header darkMode={darkMode} handleThemeChange={handleThemeChange} />
        <Container>
          <Route exact path='/' component={HomePage} />
          <Route exact path='/catalog' component={Catalog} />
          <Route path='/catalog/:id' component={ProductDetails} />
          <Route path='/about' component={AboutPage} />
          <Route path='/contact' component={ContactPage} />
          <Route path='/login' component={LoginPage} />
          {/* <Catalog /> */}

        </Container>
      </ThemeProvider>



    </>
  );
}

export default App;
