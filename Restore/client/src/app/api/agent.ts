
import axios, { AxiosResponse } from "axios";

axios.defaults.baseURL = 'https://localhost:7027/api/';

const responseBody = (response: AxiosResponse) => response.data;


// // error handler 

// axios.interceptors.response.use(response => {

//     return response
// }, (error: AxiosError) => {
//     console.log("cought by interception");
//     return Promise.reject(error.response);
// })


const request = {
    get: (url: string) => axios.get(url).then(responseBody),
    post: (url: string, body: {}) => axios.post(url, body).then(responseBody),
    put: (url: string, body: {}) => axios.put(url, body).then(responseBody),
    delete: (url: string) => axios.delete(url).then(responseBody),
}

const Catalog = {
    list: () => request.get('product'),
    details: (id: number) => request.get(`product/${id}`)
}

const agent = {
    Catalog
}

export default agent;